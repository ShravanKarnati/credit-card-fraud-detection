#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import sklearn
import matplotlib.pyplot as plt
import seaborn as sns


# In[2]:


dataset = pd.read_csv(r"creditcard.csv")


# In[3]:


dataset.head()


# In[4]:


dataset.describe()


# In[5]:


fraud = (dataset.Class == 1)
dataset.Time[fraud].describe()


# In[6]:


normal = (dataset.Class == 0)
dataset.Time[normal].describe()


# In[7]:


greenCol = '#0AFF16'
redCol = '#FF0000'
colors = [greenCol,redCol]
ax = sns.countplot(x="Class", data=dataset,palette = colors)
print('No Frauds', round(dataset['Class'].value_counts()[0]/len(dataset) * 100,2), '% of the dataset')
print('Frauds', round(dataset['Class'].value_counts()[1]/len(dataset) * 100,2), '% of the dataset')


# In[8]:


fig, axs = plt.subplots(2, 1, figsize=(20, 10), sharex=True)#,tight_layout=True)

axs[0].hist(dataset.Time[fraud],bins = 50,color = "red")
axs[0].set_title("Fraud")
axs[1].hist(dataset.Time[normal],bins = 50,color = "green")
axs[1].set_title("Normal")

plt.xlabel("Time")
plt.ylabel("No.of transactions")
plt.show()


# In[9]:


f, ax1 = plt.subplots(1, 1, figsize=(20,10))

# Entire DataFrame
corr = dataset.corr()
sns.heatmap(corr, cmap='coolwarm_r', annot_kws={'size':20}, ax=ax1)
ax1.set_title("Imbalanced Data Correlation Matrix \n", fontsize=14)

plt.show()


# # UnderSampling

# In[10]:


df = dataset.sample(frac=1)

# amount of fraud classes 492 rows.
fraud_df = df.loc[df['Class'] == 1]
# amount of fraud classes 1000 rows.
non_fraud_df = (df.loc[df['Class'] == 0]).sample(n=1000)

normal_distributed_df = pd.concat([fraud_df, non_fraud_df])

# Shuffle dataframe rows
new_df = normal_distributed_df.sample(frac=1, random_state=42)

new_df.head()


# In[11]:


print(new_df['Class'].value_counts()/len(new_df)*100,)



sns.countplot('Class', data=new_df, palette=colors)
plt.title('Undersampled Data Distributions', fontsize=14)
plt.show()


# In[12]:


f, ax2 = plt.subplots(1, 1, figsize=(20,10))


sub_sample_corr = new_df.corr()
sns.heatmap(sub_sample_corr, cmap='coolwarm_r', annot_kws={'size':20}, ax=ax2)
ax2.set_title('SubSample Correlation Matrix \n (use for reference)', fontsize=14)
plt.show()


# In[13]:


new_df = new_df[["V2",'V4','V11','V19','V20','V21','Class']].copy()
new_df


# In[14]:


X = new_df.drop('Class', axis=1)
y = new_df['Class']


# In[15]:


# Our data is already scaled we should split our training and test sets
from sklearn.model_selection import train_test_split

# This is explicitly used for undersampling.
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# In[16]:


# Turn the values into an array for feeding the classification algorithms.
X_train = X_train.values
X_test = X_test.values
y_train = y_train.values
y_test = y_test.values


# # Logistic Regression On Undersampled Data

# In[17]:


from sklearn.linear_model import LogisticRegression
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
classifiers = {
    "LogisiticRegression": LogisticRegression(),
}


# In[18]:


# Wow our scores are getting even high scores even when applying cross validation.
from sklearn.model_selection import cross_val_score


for key, classifier in classifiers.items():
    classifier.fit(X_train, y_train)
    training_score = cross_val_score(classifier, X_train, y_train, cv=5)
    print( classifier.__class__.__name__, "Has a training score of", round(training_score.mean(), 2) * 100, "% accuracy score")


# In[19]:


# Use GridSearchCV to find the best parameters.
from sklearn.model_selection import GridSearchCV


# Logistic Regression 
log_reg_params = {"penalty": ['l1', 'l2'], 'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000]}



grid_log_reg = GridSearchCV(LogisticRegression(), log_reg_params)
grid_log_reg.fit(X_train, y_train)
# We automatically get the logistic regression with the best parameters.
log_reg = grid_log_reg.best_estimator_

log_reg


# In[20]:


log_reg_score = cross_val_score(log_reg, X_train, y_train, cv=5)
print('Logistic Regression Cross Validation Score: ', round(log_reg_score.mean() * 100, 2).astype(str) + '%')


# In[21]:


log_reg_score = cross_val_score(log_reg, X_test, y_test, cv=5)
print('Logistic Regression Test Score: ', round(log_reg_score.mean() * 100, 2).astype(str) + '%')


# In[22]:


# Let's Plot LogisticRegression Learning Curve
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import learning_curve

def plot_learning_curve(estimator1, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    f, ax1 = plt.subplots(1,1, figsize=(20,14), sharey=True)
    if ylim is not None:
        plt.ylim(*ylim)
    # First Estimator
    train_sizes, train_scores, test_scores = learning_curve(
        estimator1, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    ax1.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="#ff9124")
    ax1.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="#2492ff")
    ax1.plot(train_sizes, train_scores_mean, 'o-', color="#ff9124",
             label="Training score")
    ax1.plot(train_sizes, test_scores_mean, 'o-', color="#2492ff",
             label="Cross-Validation score")
#     ax1.plot(train_sizes, test_scores_mean, 'o-', color="#2492ff",
#              label="Test score")
    ax1.set_title("Logistic Regression Learning Curve", fontsize=14)
    ax1.set_xlabel('Training size (m)')
    ax1.set_ylabel('Score')
    ax1.grid(True)
    ax1.legend(loc="best")
    return plt


# In[23]:


cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=42)
plot_learning_curve(log_reg,X_train, y_train, (0.87, 1.01), cv=cv, n_jobs=4)


# In[24]:


from sklearn.metrics import confusion_matrix
y_true = y_test
y_pred = log_reg.predict(X_test)


# In[25]:


cm_ulr = confusion_matrix(y_true, y_pred)
print(cm_ulr)


# In[26]:


import itertools

# Create a confusion matrix
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

#     print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title, fontsize=14)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    


# In[27]:


# labels = ['No Fraud', 'Fraud']

# plot_confusion_matrix(cm_ulr, labels, title="Random UnderSample \n Confusion Matrix", cmap=plt.cm.Reds)


# In[28]:


confusion_matrix(y_true, y_true)


# In[29]:


total_ulr_test = cm_ulr[0,0]+cm_ulr[0,1]+cm_ulr[1,0]+cm_ulr[1,1]
accuracy_ulr_test=(cm_ulr[0,0]+cm_ulr[1,1])/total_ulr_test
print ('Accuracy : ', accuracy_ulr_test * 100 ,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity_ulr_test = cm_ulr[0,0]/(cm_ulr[0,0]+cm_ulr[0,1])
print('Sensitivity : ', sensitivity_ulr_test * 100 ,"%")
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity_ulr_test = cm_ulr[1,1]/(cm_ulr[1,0]+cm_ulr[1,1])
print('Specificity : ', specificity_ulr_test * 100 ,"%")


# In[30]:


X34 = dataset.drop('Class', axis=1)
y34 = dataset['Class']
log_reg_score = cross_val_score(log_reg, X34, y34)


# In[31]:


y_true34 = y34
X34 = X34[['V2','V4','V11','V19','V20','V21']].copy()
y_pred34 = log_reg.predict(X34)
cm_d = confusion_matrix(y_true34, y_pred34)
print(cm_d)


# In[32]:


total1 = cm_d[0,0]+cm_d[0,1]+cm_d[1,0]+cm_d[1,1]
accuracy1=(cm_d[0,0]+cm_d[1,1])/total1
print ('Accuracy : ', accuracy1 * 100 ,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity1 = cm_d[0,0]/(cm_d[0,0]+cm_d[0,1])
print('Sensitivity : ', sensitivity1 * 100 ,"%")
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity1 = cm_d[1,1]/(cm_d[1,0]+cm_d[1,1])
print('Specificity : ', specificity1 * 100 ,"%")


# In[ ]:





# In[ ]:





# # Neural Network On Undersampled Data

# In[33]:


import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Activation,Dropout,Flatten
from keras.layers.core import Dense
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy

n_inputs = X_train.shape[1]

undersample_model = Sequential()
undersample_model.add(Dense(activation = "relu",units = n_inputs,input_shape=(n_inputs,)))
undersample_model.add(Dense(activation = "relu",units = 28))
undersample_model.add(Dense(activation = "relu",units = 28))
undersample_model.add(Dropout(0.2))
undersample_model.add(Dense(activation = "softmax",units = 2))
undersample_model.summary()


# In[34]:


undersample_model.compile(Adam(lr=0.001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
undersample_model.fit(X_train, y_train, validation_split=0.2, batch_size=25, epochs=10, shuffle=True, verbose=2)


# In[35]:


loss,acc = undersample_model.evaluate(X_test, y_test)
print("Accuracy on Test Data : " ,acc*100,"%")


# In[36]:


from sklearn.metrics import confusion_matrix
y_true = y_test
y_pred = undersample_model.predict_classes(X_test).round()
cm_unn = confusion_matrix(y_true, y_pred)
print(cm_unn)


# In[37]:


total1 = cm_unn[0,0]+cm_unn[0,1]+cm_unn[1,0]+cm_unn[1,1]
accuracy1=(cm_unn[0,0]+cm_unn[1,1])/total1
print ('Accuracy : ', accuracy1 * 100 ,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity1 = cm_unn[0,0]/(cm_unn[0,0]+cm_unn[0,1])
print('Sensitivity : ', sensitivity1 * 100 ,"%")
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity1 = cm_unn[1,1]/(cm_unn[1,0]+cm_unn[1,1])
print('Specificity : ', specificity1 * 100 ,"%")


# In[38]:


y_true33 = dataset['Class']
X_test33 = dataset[['V2','V4','V11','V19','V20','V21']].copy()
y_pred33 = undersample_model.predict_classes(X_test33).round()
cm_dnn = confusion_matrix(y_true33, y_pred33)
print(cm_dnn)


# In[39]:


total1 = cm_dnn[0,0]+cm_dnn[0,1]+cm_dnn[1,0]+cm_dnn[1,1]
accuracy1=(cm_dnn[0,0]+cm_dnn[1,1])/total1
print ('Accuracy : ', accuracy1*100,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity1 = cm_dnn[0,0]/(cm_dnn[0,0]+cm_dnn[0,1])
print('Sensitivity : ', sensitivity1*100,"%" )
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity1 = cm_dnn[1,1]/(cm_dnn[1,0]+cm_dnn[1,1])
print('Specificity : ', specificity1*100,"%")


# In[ ]:





# In[ ]:





# # t-SNE Cluster Analysis 

# In[40]:


from sklearn.manifold import TSNE
from sklearn.decomposition import PCA, TruncatedSVD
import matplotlib.patches as mpatches
import time

X = new_df.drop('Class', axis=1)
y = new_df['Class']


# T-SNE Implementation
t0 = time.time()
X_reduced_tsne = TSNE(n_components=2, random_state=42).fit_transform(X.values)
t1 = time.time()
print("T-SNE took {:.2} s".format(t1 - t0))


f, ax1 = plt.subplots(1, 1, figsize=(20,6))
# labels = ['No Fraud', 'Fraud']
f.suptitle('Clusters using Dimensionality Reduction', fontsize=14)


blue_patch = mpatches.Patch(color='#0A0AFF', label='No Fraud')
red_patch = mpatches.Patch(color='#AF0000', label='Fraud')


# t-SNE scatter plot
ax1.scatter(X_reduced_tsne[:,0], X_reduced_tsne[:,1], c=(y == 0), cmap='coolwarm', label='No Fraud', linewidths=2)
ax1.scatter(X_reduced_tsne[:,0], X_reduced_tsne[:,1], c=(y == 1), cmap='coolwarm', label='Fraud', linewidths=2)
ax1.set_title('t-SNE', fontsize=14)

ax1.grid(True)

ax1.legend(handles=[blue_patch, red_patch])


plt.show()


# In[ ]:





# In[ ]:





# # OverSampling Using SMOTE

# In[41]:


# SMOTE for oversampling
from imblearn.over_sampling import SMOTE

new_df = dataset[["V2",'V4','V11','V19','V20','V21','Class']].copy()
X = new_df.drop('Class', axis=1)
y = new_df['Class']

sm = SMOTE(random_state = 12, ratio = 0.32)
X, y = sm.fit_sample(X, y)


# In[42]:


unique, count = np.unique(y, return_counts=True)
y_train_smote_value_count = { k:v for (k,v) in zip(unique, count)}
y_train_smote_value_count


# In[43]:


from sklearn.model_selection import train_test_split

# This is explicitly used for undersampling.
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# # Logistic Regression On OverSampled Data

# In[44]:


from sklearn.linear_model import LogisticRegression
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
clf = LogisticRegression().fit(X_train, y_train)


# In[45]:


Y_Test_Pred = clf.predict(X_test)


# In[46]:


from sklearn.metrics import confusion_matrix
cm_olr = confusion_matrix(y_test, Y_Test_Pred)
print(cm_olr)


# In[47]:


cm_olr1 = confusion_matrix(y_test,y_test)
print(cm_olr1)


# In[48]:


total1 = cm_olr[0,0]+cm_olr[0,1]+cm_olr[1,0]+cm_olr[1,1]
accuracy1=(cm_olr[0,0]+cm_olr[1,1])/total1
print ('Accuracy : ', accuracy1*100,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity1 = cm_olr[0,0]/(cm_olr[0,0]+cm_olr[0,1])
print('Sensitivity : ', sensitivity1*100,"%" )
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity1 = cm_olr[1,1]/(cm_olr[1,0]+cm_olr[1,1])
print('Specificity : ', specificity1*100,"%")


# In[ ]:





# In[ ]:





# # Neural Network On OverSampled Data

# In[49]:


import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Activation,Dropout,Flatten
from keras.layers.core import Dense
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy

n_inputs = X_train.shape[1]

oversample_model = Sequential()
oversample_model.add(Dense(activation = "relu",units = n_inputs,input_shape=(n_inputs,)))
oversample_model.add(Dense(activation = "relu",units = 28))
oversample_model.add(Dense(activation = "relu",units = 28))
oversample_model.add(Dropout(0.2))
oversample_model.add(Dense(activation = "softmax",units = 2))
oversample_model.summary()


# In[50]:


oversample_model.compile(Adam(lr=0.001), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
oversample_model.fit(X_train, y_train, validation_split=0.2, batch_size=25, epochs=10, shuffle=True, verbose=2)


# In[51]:


loss,acc = oversample_model.evaluate(X_test, y_test)
print("Accuracy on Test Data : " ,acc*100,"%")


# In[52]:


from sklearn.metrics import confusion_matrix
y_true = y_test
y_pred = undersample_model.predict_classes(X_test).round()
cm_onn = confusion_matrix(y_true, y_pred)
print(cm_onn)


# In[53]:


total1 = cm_onn[0,0]+cm_onn[0,1]+cm_onn[1,0]+cm_onn[1,1]
accuracy1=(cm_onn[0,0]+cm_onn[1,1])/total1
print ('Accuracy : ', accuracy1*100,"%")
# True Positive Rate(Sensitivity or Recall): When it’s actually yes, how often does it predict yes?
sensitivity1 = cm_onn[0,0]/(cm_onn[0,0]+cm_onn[0,1])
print('Sensitivity : ', sensitivity1*100,"%" )
# True Negative Rate(Specificity): When it’s actually no, how often does it predict no?
specificity1 = cm_onn[1,1]/(cm_onn[1,0]+cm_onn[1,1])
print('Specificity : ', specificity1*100,"%")


# In[ ]:





# In[ ]:





# In[ ]:




